package collections;

import java.util.ArrayList;
import java.util.Collections;
/**
 * Polynomial Class which creates the polynomial from the Terms
 * @author Safiyyah
 *
 */
public class Polynomial {
	/**
	 * creates a generic arraylist containing Term objects
	 */
	private ArrayList<Term> polynomial; 
	public Polynomial() {
		//ArrayList<Term> poly = new ArrayList<Term>();
		polynomial = new ArrayList<Term>();
	}
	/**
	 * insert method first checks if the arraylist is empty. if it is it
	 * puts the new term in the 0th position.
	 * insert method then inserts a new term in the arraylist, sorted by 
	 * its exponent. 
	 * @param T - the term to be inserted in the arraylist
	 */
	public void insert(Term T)
	{
		Term term = T;
		if(polynomial.isEmpty())
		{
			polynomial.add(0, T);			
		}
		else{
			for(int i = 0; i < polynomial.size(); i++)
			{
				if(polynomial.get(i).getExponent() > term.getExponent())
				{
					Term newT = polynomial.set(i, term);
					term = newT;
				}
			}
			polynomial.add(term);
		}
	}
	/**
	 * delete method removes a term from polynomial expression.
	 * if the term is not in the expression, it prints out a statement saying so.
	 * @param T
	 */
	public void delete(Term T)
	{
		boolean wasRemoved = false;
		for(int i = 0; i < polynomial.size(); i++)
		{
			if(polynomial.get(i).getCoefficient() == T.getCoefficient() && polynomial.get(i).getExponent() == T.getExponent())
			{
				polynomial.remove(i);
				wasRemoved = true;
			}
		}
		if(!wasRemoved){
			System.out.println("The term was not deleted because it was not in polynomial");
		}
	}
	/**
	 * Reverse the order of the terms in the polynomial using the built in reverse function 
	 * for collections
	 */
	public void reverse()
	{
		Collections.reverse(polynomial);
	}
	/**
	 * calculates the product of the terms and returns in the form of a string
	 * @return
	 */
	public String product()
	{
		int prodEx = 1;
		int prodCo = 1;
		int prod = 1;
		for(int i = 0; i < polynomial.size(); i++)
		{
			prodCo = polynomial.get(i).getCoefficient() * prodCo;
		}
		for(int i = 0; i < polynomial.size(); i++)
		{
			prodEx = polynomial.get(i).getExponent() * prodEx;
		}
		prod = prodCo * prodEx;
		//String s = "";
		return String.valueOf(prod);
			
	}

}
