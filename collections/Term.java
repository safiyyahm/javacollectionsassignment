package collections;

/**
 * Term class which represents term in algebraic expression
 * @author Safiyyah	
 *
 */
public class Term {
	/**
	 * instantiates instance variables & makes them private
	 */
	private int coefficient;
	private int exponent; 
	/**
	 * Constructor for term class w/ 2 parameters
	 * @param c - represents coefficient variable
	 * @param e - represents exponent variable
	 */
	public Term(int c, int e) {
		coefficient = c;
		exponent = e;
	}
	/**
	 * Accessor method for coefficient
	 * @return - coefficient 
	 */
	public int getCoefficient()
	{
		return coefficient;
	}
	/**
	 * Accessor method for exponent
	 * @return - exponent
	 */
	public int getExponent()
	{
		return exponent;
	}
	/**
	 * Converts coefficient and exponent into algebraic representation (string form)
	 */
	public String toString()
	{
		String co = String.valueOf(coefficient);
		String ex = String.valueOf(exponent);
		
		if(exponent == 0)
		{
			return co;
		}
		else if(exponent == 1)
		{
			return co + "x";
		}
		
		return co + "x^" + ex;
	}

}
