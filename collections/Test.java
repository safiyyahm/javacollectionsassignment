package collections;

import collections.Polynomial;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.*;

/**
 * 
 * This class functions as the driver class. It reads in a file and will perform various functions
 * on the input from that file.
 * @author Safiyyah
 *
 */
public class Test {
	public static void main(String[] args) throws Exception
	{
		Polynomial new_Polynomial = new Polynomial();
		FileInputStream file;
		BufferedReader read;
		
		file = new FileInputStream("input.txt");
		read = new BufferedReader(new InputStreamReader(file));
		
		String text = read.readLine();
		
		while(text != null)
		{
			String arr[] = text.split(";");
		}
	}
}
